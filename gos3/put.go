// SPDX-License-Identifier: BSD-3-Clause

/* Put object functionality
 * ------------------------
 * Puts a single item into an S3 bucket
 */

package gos3

import (
	"bytes"
	"net/http"
)

// Put method puts an object that is in memory into an S3 bucket
func (s3 GoS3) Put(bucket, key string, buf []byte) error {
	// Create a PUT request
	if s3.client == nil {
		s3.init()
	}
	req, err := http.NewRequest("PUT", "https://"+s3.BaseDomain+"/"+bucket+"/"+key, bytes.NewBuffer(buf))
	if err != nil {
		return err
	}

	s3.buildheader(bucket, key, "", "", req, buf)

	// Do the request
	resp, err := s3.client.Do(req)

	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return &ResponseError{"Could not upload object", resp.StatusCode}
	}
	return nil
}
