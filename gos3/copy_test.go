// SPDX-License-Identifier: BSD-3-Clause
package gos3

import (
	"bytes"
	"os"
	"testing"
)

// Test does following:
// 1. puts an item int S3 (S3CopyText.txt)
// 2. retrieves it
// 3. compares the contents
// 4. makes a copy in the same bucket (S3CopyNewText.txt)
// 5. retrieves the copy
// 6. compares the copy contents
// 7. deletes both the original and the copy
func TestCopy(t *testing.T) {
	s3key := os.Getenv("S3KEY")
	s3secret := os.Getenv("S3SECRET")
	s3region := os.Getenv("S3REGION")
	s3bucket := os.Getenv("S3BUCKET")
	s3host := os.Getenv("S3HOST")

	if s3secret == "" || s3key == "" || s3region == "" || s3host == "" {
		t.Skip("A required enviroment variable is missing")
	}

	//s3 := NewGoS3(s3key, s3secret, s3region, s3host)

	s3 := &GoS3{
		Key:        s3key,
		Secret:     s3secret,
		Region:     s3region,
		BaseDomain: s3host,
	}

	testdata := []byte("Hello world!")
	err := s3.Put(s3bucket, "S3CopyText.txt", testdata)

	if err != nil {
		t.Errorf(err.Error())
	}

	r, err := s3.Get(s3bucket, "S3CopyText.txt")

	if err != nil {
		t.Errorf(err.Error())
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(r)
	if bytes.Compare(buf.Bytes(), testdata) != 0 {
		t.Errorf("Data compare fail")

	}

	err = s3.Copy(s3bucket, "S3CopyNewText.txt", s3bucket, "S3CopyText.txt")

	if err != nil {
		t.Errorf(err.Error())
	}

	r, err = s3.Get(s3bucket, "S3CopyNewText.txt")

	if err != nil {
		t.Errorf(err.Error())
	}
	buf = new(bytes.Buffer)
	buf.ReadFrom(r)
	if bytes.Compare(buf.Bytes(), testdata) != 0 {
		t.Errorf("Data compare fail")

	}

	err = s3.Delete(s3bucket, "S3CopyNewText.txt")
	if err != nil {
		t.Errorf(err.Error())
	}

	err = s3.Delete(s3bucket, "S3CopyText.txt")
	if err != nil {
		t.Errorf(err.Error())
	}
}
