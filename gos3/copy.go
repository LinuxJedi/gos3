// SPDX-License-Identifier: BSD-3-Clause

/* Copy object functionality
 * -------------------------
 * Copies a single item from one bucket/key to another bucket/key
 */

package gos3

import (
	"net/http"
)

// Copy method copies an object from a bucket/key to a bucket/key
func (s3 GoS3) Copy(bucket, key, sourceBucket, sourceKey string) error {
	// Create a PUT request
	if s3.client == nil {
		s3.init()
	}
	req, err := http.NewRequest("PUT", "https://"+s3.BaseDomain+"/"+bucket+"/"+key, nil)
	if err != nil {
		return err
	}

	s3.buildheader(bucket, key, sourceBucket, sourceKey, req, nil)

	// Do the request
	resp, err := s3.client.Do(req)

	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return &ResponseError{"Could not upload object", resp.StatusCode}
	}
	return nil
}
