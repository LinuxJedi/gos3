// SPDX-License-Identifier: BSD-3-Clause

/* List bucket functionality
 * -------------------------
 * Only supporting S3 v1 list for now.
 * Takes the XML response for a GET on a bucket path and converts it into
 * an array of "List" items to return
 * TODO items:
 * * get next list in truncated list
 * * list prefix search
 * * maybe v2 list
 * * filter "directories" option
 */

package gos3

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"time"
)

// List struct used in return for List method
type List struct {
	Key     string    // The object / file name
	Size    uint64    // The object size
	Created time.Time // The created time stamp
}

// Next two strycts are v1 response parts that we care about
type listbucketresponse struct {
	XMLName   xml.Name   `xml:"ListBucketResult"`
	Contents  []contents `xml:"Contents"`
	Truncated bool       `xml:"IsTruncated"`
}

type contents struct {
	Key          string `xml:"Key"`
	LastModified string `xml:"LastModified"`
	Size         uint64 `xml:"Size"`
}

// List method lists the content of a given bucket. It is currently limited
// to the first 1000 objects
func (s3 GoS3) List(bucket string) (*[]List, error) {
	if s3.client == nil {
		s3.init()
	}
	req, err := http.NewRequest("GET", "https://"+s3.BaseDomain+"/"+bucket, nil)
	if err != nil {
		return nil, err
	}

	s3.buildheader(bucket, "", "", "", req, nil)

	// Do request
	resp, err := s3.client.Do(req)

	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, &ResponseError{"Could not list bucket", resp.StatusCode}
	}

	// Read all the response
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	// Parse the response XML
	var listresponse listbucketresponse
	err = xml.Unmarshal(body, &listresponse)

	if err != nil {
		return nil, err
	}

	// Add the XML content items to the list array
	list := make([]List, 0)
	for _, content := range listresponse.Contents {
		var item List
		item.Key = content.Key
		item.Size = content.Size
		item.Created, err = time.Parse("2006-01-02T15:04:05Z", content.LastModified)
		if err != nil {
			return nil, err
		}
		list = append(list, item)
	}

	return &list, err
}
