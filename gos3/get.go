// SPDX-License-Identifier: BSD-3-Clause

/* Get object functionality
 * ------------------------
 * Gets a single item from an S3 bucket and returns a reader for it
 */

package gos3

import (
	"bufio"
	"net/http"
)

// Get retrieves an object from S3 and returns the Reader instance for the data (or nil on error)
func (s3 GoS3) Get(bucket, key string) (*bufio.Reader, error) {
	// Create a GET request
	if s3.client == nil {
		s3.init()
	}
	req, err := http.NewRequest("GET", "https://"+s3.BaseDomain+"/"+bucket+"/"+key, nil)
	if err != nil {
		return nil, err
	}

	s3.buildheader(bucket, key, "", "", req, nil)

	// Do the request and return the reader
	resp, err := s3.client.Do(req)
	reader := bufio.NewReader(resp.Body)
	if err == nil && resp.StatusCode != 200 {
		return nil, &ResponseError{"Could not get object", resp.StatusCode}
	}
	return reader, err
}
