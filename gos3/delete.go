// SPDX-License-Identifier: BSD-3-Clause

/* Delete object functionality
 * ---------------------------
 * Deletes an object from an S3 bucket
 */

package gos3

import (
	"net/http"
)

// Delete method deletes an object from an S3 bucket. It should be noted that
// a non-existing file will still cause a "success" state.
func (s3 GoS3) Delete(bucket, key string) error {
	// Create a PUT request
	if s3.client == nil {
		s3.init()
	}
	req, err := http.NewRequest("DELETE", "https://"+s3.BaseDomain+"/"+bucket+"/"+key, nil)
	if err != nil {
		return err
	}

	s3.buildheader(bucket, key, "", "", req, nil)

	// Do the request
	resp, err := s3.client.Do(req)

	if err != nil {
		return err
	}

	// Google's implementation of S3 gives a 404 if not found, everyone
	// else just gives 204 for success or fail
	if resp.StatusCode != 204 && resp.StatusCode != 404 {
		return &ResponseError{"Could not delete object", resp.StatusCode}
	}
	return nil
}
