// SPDX-License-Identifier: BSD-3-Clause
package gos3

import (
	"os"
	"testing"
)

// Test retrieving a bad item
func TestStatusFail(t *testing.T) {
	s3key := os.Getenv("S3KEY")
	s3secret := os.Getenv("S3SECRET")
	s3region := os.Getenv("S3REGION")
	s3bucket := os.Getenv("S3BUCKET")
	s3host := os.Getenv("S3HOST")

	if s3secret == "" || s3key == "" || s3region == "" || s3host == "" {
		t.Skip("A required enviroment variable is missing")
	}

	//s3 := NewGoS3(s3key, s3secret, s3region, s3host)

	s3 := &GoS3{
		Key:        s3key,
		Secret:     s3secret,
		Region:     s3region,
		BaseDomain: s3host,
	}

	_, err := s3.Status(s3bucket, "NoSuchItem.txt")

	switch err.(type) {
	case nil:
		t.Errorf("Error was expected but didn't happen")
	case *ResponseError:
		errVal := err.(*ResponseError)
		if errVal.Code != 404 {
			t.Errorf(err.Error())
		}
		return
	default:
		t.Errorf(err.Error())
	}
}
