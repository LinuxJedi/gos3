// SPDX-License-Identifier: BSD-3-Clause

/* Object status functionality
 * ---------------------------
 * Gets information about an item in an S3 bucket
 */

package gos3

import (
	"net/http"
	"strconv"
	"time"
)

// Status response struct
type Status struct {
	Modified time.Time // Object last modified time
	Size     uint64    // Object size
}

// Status method retrieves the status about an object from S3
func (s3 GoS3) Status(bucket, key string) (*Status, error) {
	// Create a HEAD request
	if s3.client == nil {
		s3.init()
	}
	req, err := http.NewRequest("HEAD", "https://"+s3.BaseDomain+"/"+bucket+"/"+key, nil)
	if err != nil {
		return nil, err
	}

	s3.buildheader(bucket, key, "", "", req, nil)

	// Do the request and return the reader
	resp, err := s3.client.Do(req)
	if err == nil && resp.StatusCode != 200 {
		return nil, &ResponseError{"Could not get object", resp.StatusCode}
	}
	// For now we only care about Last-Modified and Content-Length headers
	var status Status
	timestamp := resp.Header.Get("Last-Modified")
	status.Modified, err = time.Parse("Mon, 02 Jan 2006 15:04:05 MST", timestamp)
	if err != nil {
		return nil, err
	}
	status.Size, err = strconv.ParseUint(resp.Header.Get("Content-Length"), 10, 64)

	return &status, err
}
