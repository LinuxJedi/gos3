// SPDX-License-Identifier: BSD-3-Clause

// Package gos3 implements an easy to use interface to use Amazon S3 and compatible servers
package gos3

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net/http"
	"time"
)

// ResponseError contains HTTP error details
type ResponseError struct {
	Message string
	Code    int
}

func (err *ResponseError) Error() string {
	return fmt.Sprintf("%s (code %d)", err.Message, err.Code)
}

// GoS3 public struct
type GoS3 struct {
	Key        string // The authentication key for S3
	Secret     string // The authentication secret for S3
	Region     string // The region in S3 to use
	BaseDomain string // The domain/IP to connect to, "" will connect to Amazon's S3
	client     *http.Client
}

// NewGoS3 A helper to create a new GoS3 instance
func NewGoS3(key, secret, region string) *GoS3 {
	s3 := &GoS3{
		Key:    key,
		Secret: secret,
		Region: region,
	}
	s3.init()
	return s3
}

func (s3 *GoS3) init() {
	// Create an instance of http client so that a connection is reused across
	// requests
	s3.client = &http.Client{
		Timeout: time.Second * 10,
	}
	if s3.BaseDomain == "" {
		s3.BaseDomain = "s3.amazonaws.com"
	}
}

func (s3 GoS3) buildheader(bucket, key, sourceBucket, sourceKey string, req *http.Request, buf []byte) {
	// Set the hostname header
	req.Header.Add("host", s3.BaseDomain)

	// If this was a PUT request we would sha256 the data.
	postSum := sha256.New()
	postSum.Write(buf)
	sumEncoded := hex.EncodeToString(postSum.Sum(nil))
	req.Header.Add("x-amz-content-sha256", sumEncoded)

	// For COPY command we need the source
	var headerList string
	if sourceBucket != "" {
		req.Header.Add("x-amz-copy-source", "/"+sourceBucket+"/"+sourceKey)
		headerList = "host;x-amz-content-sha256;x-amz-copy-source;x-amz-date"
	} else {
		headerList = "host;x-amz-content-sha256;x-amz-date"
	}

	// Use UTC time for requests and set the date header
	nowTime := time.Now().UTC()
	req.Header.Add("x-amz-date", nowTime.Format("20060102T150405Z"))

	// Details of the request are built and turned into a sha256
	var signingData bytes.Buffer
	signingData.WriteString(req.Method + "\n")
	// key is not used in List
	if key != "" {
		signingData.WriteString("/" + bucket + "/" + key + "\n")
	} else {
		signingData.WriteString("/" + bucket + "\n")
	}

	signingData.WriteString("\n") // Query
	signingData.WriteString("host:" + req.Header.Get("host") + "\n")
	signingData.WriteString("x-amz-content-sha256:" + req.Header.Get("x-amz-content-sha256") + "\n")
	if sourceBucket != "" {
		signingData.WriteString("x-amz-copy-source:" + req.Header.Get("x-amz-copy-source") + "\n")
	}
	signingData.WriteString("x-amz-date:" + req.Header.Get("x-amz-date") + "\n\n")
	signingData.WriteString(headerList + "\n")
	signingData.WriteString(sumEncoded)
	reqSum := sha256.New()
	reqSum.Write(signingData.Bytes())
	reqSumEncoded := hex.EncodeToString(reqSum.Sum(nil))

	// Build an HMAC for the request signed with the secret key
	// Every part of this is an HMAC with the previous entry as the key
	signHash := hmac.New(sha256.New, []byte("AWS4"+s3.Secret))
	signHash.Write([]byte(nowTime.Format("20060102")))
	signHash = hmac.New(sha256.New, signHash.Sum(nil))
	signHash.Write([]byte(s3.Region))
	signHash = hmac.New(sha256.New, signHash.Sum(nil))
	signHash.Write([]byte("s3"))
	signHash = hmac.New(sha256.New, signHash.Sum(nil))
	signHash.Write([]byte("aws4_request"))

	// Further request details that need to be added to the HMAC
	// This is all one block
	signHash = hmac.New(sha256.New, signHash.Sum(nil))
	signHash.Write([]byte("AWS4-HMAC-SHA256\n"))
	signHash.Write([]byte(nowTime.Format("20060102T150405Z") + "\n"))
	signHash.Write([]byte(nowTime.Format("20060102") + "/" + s3.Region + "/s3/aws4_request\n"))
	// The signature for the request here
	signHash.Write([]byte(reqSumEncoded))

	// Authorization header including the final HMAC

	req.Header.Add("Authorization", "AWS4-HMAC-SHA256 Credential="+s3.Key+"/"+nowTime.Format("20060102")+"/"+s3.Region+"/s3/aws4_request, SignedHeaders="+headerList+", Signature="+hex.EncodeToString(signHash.Sum(nil)))
}
