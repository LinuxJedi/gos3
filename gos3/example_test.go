// SPDX-License-Identifier: BSD-3-Clause

package gos3_test

import (
	"bytes"
	"fmt"
	"gos3/gos3"
	"log"
	"os"
	"time"
)

// This example puts an item int S3, retrieves it, compares the contents and
// then deletes it
func ExampleGoS3() {
	// Get connection details from environment
	s3key := os.Getenv("S3KEY")
	s3secret := os.Getenv("S3SECRET")
	s3region := os.Getenv("S3REGION")
	s3bucket := os.Getenv("S3BUCKET")
	s3host := os.Getenv("S3HOST")

	if s3secret == "" || s3key == "" || s3region == "" {
		log.Fatal("A required enviroment variable is missing")
	}

	// Create an instance of GoS3, we could also use NewGoS3()
	s3 := &gos3.GoS3{
		Key:        s3key,
		Secret:     s3secret,
		Region:     s3region,
		BaseDomain: s3host,
	}

	// Create some test data
	testdata := []byte("Hello world!")

	// Put the test data into S3
	err := s3.Put(s3bucket, "S3GetText.txt", testdata)

	// If there is an error writing the object, fatal log it
	if err != nil {
		log.Fatal(err.Error())
	}

	// Retrieve the same object from S3
	r, err := s3.Get(s3bucket, "S3GetText.txt")

	// Check for an error retrieving the file
	if err != nil {
		log.Fatal(err.Error())
	}

	// Retrieve the data into a byte buffer
	buf := new(bytes.Buffer)
	buf.ReadFrom(r)

	// Compare the output with what was originally writen
	if bytes.Compare(buf.Bytes(), testdata) != 0 {
		log.Fatal("Data compare fail")
	}

	fmt.Printf(buf.String())

	// Delete the file from S3
	err = s3.Delete(s3bucket, "S3GetText.txt")
	if err != nil {
		log.Fatal(err.Error())
	}

	// Output:
	// Hello world!
}

// The example puts an item to S3, executes a list to find it, checks the
// timestamp, returns the size and then deletes it
func ExampleGoS3_List() {
	// Get the connection details from environment variables
	s3key := os.Getenv("S3KEY")
	s3secret := os.Getenv("S3SECRET")
	s3region := os.Getenv("S3REGION")
	s3bucket := os.Getenv("S3BUCKET")
	s3host := os.Getenv("S3HOST")

	if s3secret == "" || s3key == "" || s3region == "" {
		log.Fatal("A required enviroment variable is missing")
	}

	s3 := gos3.NewGoS3(s3key, s3secret, s3region)
	s3.BaseDomain = s3host

	// Create some test data and save it in an object
	testdata := []byte("Hello world!")
	err := s3.Put(s3bucket, "S3ListText.txt", testdata)

	if err != nil {
		log.Fatal(err.Error())
	}

	// Get a list of files from the bucket
	list, err := s3.List(s3bucket)

	if err != nil {
		log.Fatalf(err.Error())
	}

	var found bool = false
	// Iterate through list
	for _, item := range *list {
		if item.Key == "S3ListText.txt" {
			// Found the item we were looking for
			found = true
			fmt.Printf("Item's size is %d", item.Size)

			// Make sure time stamp is sane
			arbitraryTime := time.Date(2019, 01, 01, 01, 01, 01, 000, time.UTC)
			if item.Created.Before(arbitraryTime) {
				log.Fatal("Created time is less than expected")
			}
			break
		}
	}

	if !found {
		log.Fatal("Could not find item")
	}
	// Delete the file
	err = s3.Delete(s3bucket, "S3ListText.txt")
	if err != nil {
		log.Fatal(err.Error())
	}

	// Output:
	// Item's size is 12
}

// This example puts an item int S3, retrieves the status, and then deletes it
func ExampleGoS3_Status() {
	s3key := os.Getenv("S3KEY")
	s3secret := os.Getenv("S3SECRET")
	s3region := os.Getenv("S3REGION")
	s3bucket := os.Getenv("S3BUCKET")
	s3host := os.Getenv("S3HOST")

	if s3secret == "" || s3key == "" || s3region == "" {
		log.Fatal("A required enviroment variable is missing")
	}

	s3 := &gos3.GoS3{
		Key:        s3key,
		Secret:     s3secret,
		Region:     s3region,
		BaseDomain: s3host,
	}

	// Create test data
	testdata := []byte("Hello world!")
	err := s3.Put(s3bucket, "S3StatusText.txt", testdata)

	if err != nil {
		log.Fatal(err.Error())
	}

	// Get the status of a file
	status, err := s3.Status(s3bucket, "S3StatusText.txt")

	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Printf("File size: %d", status.Size)

	// A check to see if the modified time for the object is sane
	arbitraryTime := time.Date(2019, 01, 01, 01, 01, 01, 000, time.UTC)
	if status.Modified.Before(arbitraryTime) {
		log.Fatal("Modified time is less than expected")
	}

	err = s3.Delete(s3bucket, "S3StatusText.txt")
	if err != nil {
		log.Fatal(err.Error())
	}

	// Output:
	// File size: 12
}
