# gos3

A project to help me learn Go, a module that provides a simple Go API to Amazon S3 (and compatible)

## Testing

```bash
go test -v ./gos3
```

## Using test

Set the following environment variables:

| Variable   | Desription                                               |
|------------|----------------------------------------------------------|
| S3KEY      | Your AWS access key                                      |
| S3SECRET   | Your AWS secret key                                      |
| S3REGION   | The AWS region (for example us-east-1)                   |
| S3BUCKET   | The S3 bucket name                                       |
| S3HOST     | OPTIONAL hostname for non-AWS S3 service                 |

### TODO

* Improve list
